buildscript {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
    dependencies {
        classpath(Deps.RootBuild.kotlinGradle)
        classpath(Deps.RootBuild.androidGradle)
        classpath(Deps.RootBuild.hiltGradle)
        classpath(Deps.RootBuild.safeArgsPlugin)
        classpath(Deps.RootBuild.jacocoPlugin)
        classpath(Deps.RootBuild.kotlinxSerialization)
    }
}

allprojects {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
