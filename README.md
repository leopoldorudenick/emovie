EMovie
======================

## Contents

- [Getting Started](#getting-started)
- [Secrets](#secrets)
- [Features](#features)
- [Development Environment](#development-environment)
- [Architecture](#architecture)
- [Kotlin](#kotlin)
- [Q&A](#Q&A)

## Getting Started

1. Clone project
2. Configure `secrets.properties` (see the Secrets section below)
3. Open Android Studio
4. Select `debug` variant for local development

## Secrets

Build secrets, like API keys, are kept in an encrypted file called `secrets.properties.enc`. Before you can build the
app, you must decrypt the file and save it locally as `secrets.properties`. The password is kept on file with the
product owner (just for this example app, use 1234)

### Decrypt `secrets.properties.enc`

    $ openssl enc -d -aes-256-cbc -base64 -in secrets.properties.enc -out secrets.properties
    enter aes-256-cbc decryption password: [on file]

### Update `secrets.properties`

1. Modify the `secrets.properties` file as necessary.
1. Encrypt `secrets.properties`.

        $ openssl enc -aes-256-cbc -base64 -in secrets.properties -out secrets.properties.enc
        enter aes-256-cbc encryption password: [on file]
        Verifying - enter aes-256-cbc encryption password: [on file]

1. Check in the modified `secrets.properties.enc` file.

## Features

The example app displays a list of movies and recommendations

## Development Environment

The app is written entirely in Kotlin and uses the Gradle build system.

To build the app, use the `gradlew build` command or use "Import Project" in Android Studio. Android Studio Arctic Fox
or newer is required and may be downloaded
[here](https://developer.android.com/studio/preview).

## Architecture

The architecture is built around
[Android Architecture Components](https://developer.android.com/topic/libraries/architecture/)
and follows the recommendations laid out in the
[Guide to App Architecture](https://developer.android.com/jetpack/docs/guide). Logic is kept away from Activities and
Fragments and moved to
[ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)s. Data is observed using
[Kotlin Flows](https://developer.android.com/kotlin/flow/stateflow-and-sharedflow)
and the [Data Binding Library](https://developer.android.com/topic/libraries/data-binding/)
binds UI components in layouts to the app's data sources.

The Repository layer handles data operations. App's data comes from [The Movie Database](https://developers.themoviedb.org/)  and is
stored locally in a
[Room](https://developer.android.com/jetpack/androidx/releases/room) database, so if the device doesn't have internet
connection, the app will display the data previously fetched.
[Glide](https://bumptech.github.io/glide/) is used to fetch images from the network using cache.

A lightweight domain layer sits between the data layer and the presentation layer, and handles discrete pieces of
business logic off the UI thread. See the `.\*UseCase.kt` files under `shared/domain` for examples.

The [Navigation component](https://developer.android.com/guide/navigation) is used to implement navigation in the app,
handling Fragment transactions and providing a consistent user experience.

Unit tests use Junit4, using kotlin extensions for test coroutines.
View [Testing Kotlin coroutines on Android](https://developer.android.com/kotlin/coroutines/test)
[Jacoco](https://github.com/arturdm/jacoco-android-gradle-plugin) provides code coverage metrics for Kotlin code via
integration with JaCoCo. Use the `gradlew  jacocoTestReportDebug` command to run the test and create the report

Dependency Injection is implemented with
[Hilt](https://developer.android.com/training/dependency-injection/hilt-android)

## Kotlin

The app is entirely written in Kotlin and uses Jetpack's
[Android Ktx extensions](https://developer.android.com/kotlin/ktx).

Asynchronous tasks are handled with
[coroutines](https://developer.android.com/kotlin/coroutines). Coroutines allow for simple and safe management of
one-shot operations as well as building and consuming streams of data using
[Kotlin Flows](https://developer.android.com/kotlin/flow).

All build scripts are written with the
[Kotlin DSL](https://docs.gradle.org/current/userguide/kotlin_dsl.html).

## Q&A

1. ¿En qué consiste el principio de responsabilidad única? ¿Cuál es su propósito?
El pricipio de responsabilidad única, o "Single Responsibility", establece que una clase debe tener una 
única responsabilidad, o, en otras palabras, una única razón para cambiar. El propósito no agregar
mas responsabilidades a una clase de las que debería tener (encapsulamiento)

2. ¿Qué características tiene, según su opinión, un “buen” código o código limpio?
Un buen código o código limpio, debe ser intuitivo, fácil de interpretar, modularizado, 
que con solo mirar el código uno pueda inferir que hace, sin necesidad de ejecutarlo. 
Para ello se pueden tener en cuenta/seguir principios como SOLID, KISS(keep it simple, stupid), 
DRY(don’t repeat yourself), que nos ayudan a organizar nuestro código, modularizarlo, dividir 
claramente las responsabilidades, usar abstracciones para que sea mas sencillo de testear, etc.



