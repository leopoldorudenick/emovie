package com.lrudenick.emovie

import com.lrudenick.shared.domain.model.*
import com.lrudenick.shared.domain.repository.MovieRepository

class MockMovieRepository(private val fetchError: Boolean = false) : MovieRepository {

    private val movie1 = Movie(
        id = 1,
        title = "Movie 1 title",
        adult = false,
        plot = "Plot of a fake movie",
        posterImg = "/img1.png",
        language = "en",
        rate = 8.1,
        releaseDate = "2022-08-10"
    )

    private val movie2 = Movie(
        id = 2,
        title = "Movie 2 title",
        adult = false,
        plot = "Plot of a fake movie",
        posterImg = "/img2.png",
        language = "en",
        rate = 8.5,
        releaseDate = "2003-08-12"
    )

    private val movieResponse = MoviesResponse(
        page = 1,
        movies = listOf(movie1, movie2)
    )

    private val movieDetail = MovieDetail(
        id = 1,
        title = "Movie 1 title",
        adult = false,
        plot = "Plot of a fake movie",
        posterImg = "/img1.png",
        language = "en",
        rate = 8.1,
        releaseDate = "2022-08-10",
        genres = "Action-Sci-fi"
    )

    private val video = Video(
        id = "1234",
        key = "adso1jsaof",
        official = true,
        site = "YouTube",
        type = "Trailer"
    )

    private val videosResponse = VideosResponse(
        id = 1,
        videos = listOf(video)
    )

    override suspend fun getUpcomingMovies(): Result<MoviesResponse> {
        return if (fetchError) {
            Result.failure(Exception("Error fetching artists"))
        } else {
            Result.success((movieResponse))
        }
    }

    override suspend fun getTopRatedMovies(): Result<MoviesResponse> {
        return if (fetchError) {
            Result.failure(Exception("Error fetching artists"))
        } else {
            Result.success((movieResponse))
        }
    }

    override suspend fun getRecommendations(movieId: Int): Result<MoviesResponse> {
        return if (fetchError) {
            Result.failure(Exception("Error fetching artists"))
        } else {
            Result.success((movieResponse))
        }
    }

    override suspend fun getMovieDetail(movieId: Int): Result<MovieDetail> {
        return if (fetchError) {
            Result.failure(Exception("Error fetching artists"))
        } else {
            Result.success(movieDetail)
        }
    }

    override suspend fun getVideos(movieId: Int): Result<VideosResponse> {
        return if (fetchError) {
            Result.failure(Exception("Error fetching artists"))
        } else {
            Result.success(videosResponse)
        }
    }
}
