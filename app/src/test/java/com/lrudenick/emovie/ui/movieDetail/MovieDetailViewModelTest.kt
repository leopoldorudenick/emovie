package com.lrudenick.emovie.ui.movieDetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.lrudenick.emovie.TestCoroutineRule
import com.lrudenick.emovie.ui.StateEvent
import com.lrudenick.shared.data.source.remote.network.ProdEnvironmentProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test


@OptIn(ExperimentalCoroutinesApi::class)
internal class MovieDetailViewModelTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var testCoroutineRule = TestCoroutineRule()

    private val movieId = 1

    @Test
    fun `fetch movie detail successful`() = runTest {
        val viewModel = createViewModel()
        viewModel.getMovieDetail(movieId)
        assertNotNull(viewModel.movie)
        assertTrue(viewModel.genres.isNotEmpty())
        assertTrue(viewModel.rate.isNotEmpty())
        assertTrue(viewModel.year.isNotEmpty())
        assertTrue(viewModel.videoKey.isNotEmpty())
        assertTrue(viewModel.state == StateEvent.LOADED)
    }

    @Test
    fun `fetch movie detail error`() = runTest {
        val viewModel = createViewModel(true)
        viewModel.getMovieDetail(movieId)
        assertNull(viewModel.movie)
        assertTrue(viewModel.genres.isEmpty())
        assertTrue(viewModel.rate.isEmpty())
        assertTrue(viewModel.year.isEmpty())
        assertTrue(viewModel.videoKey.isEmpty())
        assertTrue(viewModel.state == StateEvent.ERROR)
    }

    @Test
    fun `view movie detail`() = runTest {
        val viewModel = createViewModel()
        // Empty video id
        viewModel.viewTrailer()
        assertNull(viewModel.events.value)

        viewModel.getMovieDetail(movieId)
        viewModel.viewTrailer()
        assertTrue(viewModel.events.value?.get() is MovieDetailEvents.ViewTrailer)
    }

    @Test
    fun `get image prefix path`() = runTest {
        val viewModel = createViewModel()
        assertEquals(ProdEnvironmentProvider().invoke().imgUrlPath, viewModel.prefixPath)
    }

    private fun createViewModel(fetchError: Boolean = false): MovieDetailViewModel {
        return MovieDetailViewModel(
            movieDetailFactory = FakeMovieDetailUseFactory(fetchError),
            environmentProvider = ProdEnvironmentProvider()
        )
    }
}
