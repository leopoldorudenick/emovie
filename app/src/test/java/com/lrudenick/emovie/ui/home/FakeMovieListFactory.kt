package com.lrudenick.emovie.ui.home

import com.lrudenick.emovie.MockMovieRepository
import com.lrudenick.emovie.TestCoroutineRule
import com.lrudenick.shared.domain.use.movieList.MovieList
import com.lrudenick.shared.domain.use.movieList.MovieListFactory
import com.lrudenick.shared.domain.use.movieList.MovieListUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule

@OptIn(ExperimentalCoroutinesApi::class)
class FakeMovieListFactory(fetchError: Boolean = false) : MovieListFactory {

    private val mockMovieRepository = MockMovieRepository(fetchError)

    @get:Rule
    var testCoroutineRule = TestCoroutineRule()

    private val testDispatcher = testCoroutineRule.testDispatcher

    override fun create(out: MovieList.Out): MovieListUseCase {
        return MovieListUseCase(out, mockMovieRepository, testDispatcher)
    }
}
