package com.lrudenick.emovie.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.lrudenick.emovie.TestCoroutineRule
import com.lrudenick.emovie.ui.StateEvent
import com.lrudenick.shared.data.source.remote.network.ProdEnvironmentProvider
import com.lrudenick.shared.domain.model.MovieFilter
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test


@OptIn(ExperimentalCoroutinesApi::class)
internal class HomeViewModelTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var testCoroutineRule = TestCoroutineRule()

    @Test
    fun `fetch movies successful`() = runTest {
        val viewModel = createViewModel()
        assertTrue(viewModel.upcomingMoviesList.isNotEmpty())
        assertTrue(viewModel.topRatedMoviesList.isNotEmpty())
        assertTrue(viewModel.recommendedMoviesList.isNotEmpty())
        assertTrue(viewModel.state == StateEvent.LOADED)
    }

    @Test
    fun `fetch movies error`() = runTest {
        val viewModel = createViewModel(true)
        assertTrue(viewModel.upcomingMoviesList.isEmpty())
        assertTrue(viewModel.topRatedMoviesList.isEmpty())
        assertTrue(viewModel.recommendedMoviesList.isEmpty())
        assertTrue(viewModel.state == StateEvent.ERROR)
    }

    @Test
    fun `view movie detail`() = runTest {
        val viewModel = createViewModel()
        viewModel.viewMovieDetail(viewModel.topRatedMoviesList[0].id)
        assertTrue(viewModel.events.value?.get() is HomeEvents.ViewMovieDetail)
    }

    @Test
    fun `filter recommendations`() = runTest {
        val viewModel = createViewModel()
        viewModel.filterUpdated(MovieFilter.ALL)
        assertTrue(viewModel.recommendedMoviesList.size == 2)

        viewModel.filterUpdated(MovieFilter.IN_ENGLISH)
        assertTrue(viewModel.recommendedMoviesList.size == 2)

        viewModel.filterUpdated(MovieFilter.IN_HINDI)
        assertTrue(viewModel.recommendedMoviesList.isEmpty())

        viewModel.filterUpdated(MovieFilter.RELEASED_IN_2003)
        assertTrue(viewModel.recommendedMoviesList.size == 1)
    }

    @Test
    fun `get image prefix path`() = runTest {
        val viewModel = createViewModel()
        assertEquals(ProdEnvironmentProvider().invoke().imgUrlPath, viewModel.prefixPath)
    }


    private fun createViewModel(fetchError: Boolean = false): HomeViewModel {
        return HomeViewModel(
            movieListFactory = FakeMovieListFactory(fetchError),
            environmentProvider = ProdEnvironmentProvider()
        )
    }
}
