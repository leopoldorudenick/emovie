package com.lrudenick.emovie.ui.movieDetail

import com.lrudenick.emovie.MockMovieRepository
import com.lrudenick.emovie.TestCoroutineRule
import com.lrudenick.shared.domain.use.movieDetail.MovieDetailUse
import com.lrudenick.shared.domain.use.movieDetail.MovieDetailUseCase
import com.lrudenick.shared.domain.use.movieDetail.MovieDetailUseFactory
import com.lrudenick.shared.domain.use.movieList.MovieList
import com.lrudenick.shared.domain.use.movieList.MovieListFactory
import com.lrudenick.shared.domain.use.movieList.MovieListUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule

@OptIn(ExperimentalCoroutinesApi::class)
class FakeMovieDetailUseFactory(fetchError: Boolean = false) : MovieDetailUseFactory {

    private val mockMovieRepository = MockMovieRepository(fetchError)

    @get:Rule
    var testCoroutineRule = TestCoroutineRule()

    private val testDispatcher = testCoroutineRule.testDispatcher

    override fun create(out: MovieDetailUse.Out): MovieDetailUseCase {
        return MovieDetailUseCase(out, mockMovieRepository, testDispatcher)
    }
}
