package com.lrudenick.emovie.binding

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.lrudenick.emovie.R

@BindingAdapter("click")
fun View.bindClick(
    click: () -> Unit
) {
    setOnClickListener { click() }
}


@BindingAdapter("prefixPath", "imageUrl")
fun ImageView.loadNetworkImage(prefixPath: String, url: String?) {
    if (url.isNullOrEmpty()) return
    Glide.with(context)
        .load(prefixPath + url)
        .placeholder(R.drawable.ic_empty_image)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}
