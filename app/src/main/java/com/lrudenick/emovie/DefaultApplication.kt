package com.lrudenick.emovie

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

/**
 * Custom [Application] for tasks to be run before the creation of the first activity
 * - enables vector drawables on older platforms (< API 21) to be used within
 * [android.graphics.drawable.DrawableContainer] resources
 * - registers for activity lifecycle callbacks. View [registerActivityLifecycleCallbacks]
 */
@HiltAndroidApp
class DefaultApplication : Application() {


    override fun attachBaseContext(base: Context?) {
        application = this
        super.attachBaseContext(base)
    }

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}


lateinit var application: Application
