package com.lrudenick.emovie.ui.home

import androidx.databinding.BindingAdapter
import com.lrudenick.shared.domain.model.Movie


@BindingAdapter("movieList", "movieSelector", "prefixPath")
fun MoviesRecyclerView.bindArtistList(
    items: List<Movie>,
    movieSelector: MovieAdapter.MovieSelector,
    prefixPath: String
) {

    if (adapter == null) adapter = MovieAdapter(movieSelector, prefixPath)
    (adapter as MovieAdapter).submitList(items)
}
