package com.lrudenick.emovie.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lrudenick.emovie.databinding.ItemMovieBinding
import com.lrudenick.shared.domain.model.Movie

class MovieAdapter(private val movieSelector: MovieSelector, private val prefixPath: String) :
    ListAdapter<Movie, MovieAdapter.MovieViewHolder>(artistDiff) {

    interface MovieSelector {
        fun select(movieId: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        MovieViewHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        getItem(position)?.also { movie ->
            holder.run {
                binding.movie = movie
                binding.prefixPath = prefixPath
                itemView.setOnClickListener { movieSelector.select(movie.id) }
            }
        }
    }

    class MovieViewHolder(
        val binding: ItemMovieBinding
    ) : RecyclerView.ViewHolder(binding.root)

    companion object {
        val artistDiff = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
                oldItem == newItem
        }
    }
}
