package com.lrudenick.emovie.ui.home

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

class MoviesRecyclerView : RecyclerView {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )
}
