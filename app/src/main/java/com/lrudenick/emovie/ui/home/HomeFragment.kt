package com.lrudenick.emovie.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.lrudenick.emovie.R
import com.lrudenick.emovie.databinding.FragmentHomeBinding
import com.lrudenick.shared.domain.model.MovieFilter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val viewModel by viewModels<HomeViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = FragmentHomeBinding.inflate(inflater, container, false).apply {
        controller = viewModel
        presenter = viewModel

        filtersGroup.setOnCheckedChangeListener { _, checkedId ->
            val movieFilter = when (checkedId) {
                R.id.english -> MovieFilter.IN_ENGLISH
                R.id.hindi -> MovieFilter.IN_HINDI
                R.id.year2003 -> MovieFilter.RELEASED_IN_2003
                else -> MovieFilter.ALL
            }
            viewModel.filterUpdated(movieFilter)
        }
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.observeEvents(viewLifecycleOwner) {
            when (it) {
                is HomeEvents.ViewMovieDetail -> {
                    val action =
                        HomeFragmentDirections.actionNavigationHomeToMovieDetailFragment(it.movieId)
                    findNavController().navigate(action)
                }
            }
        }
    }
}
