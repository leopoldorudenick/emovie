package com.lrudenick.emovie.ui

/**
 * Util enum to indicate the different process states
 */
enum class StateEvent {
    IDLE, LOADING, ERROR, LOADED
}
