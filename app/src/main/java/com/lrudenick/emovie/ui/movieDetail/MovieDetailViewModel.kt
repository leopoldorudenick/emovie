package com.lrudenick.emovie.ui.movieDetail

import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.lrudenick.emovie.BR
import com.lrudenick.emovie.ui.StateEvent
import com.lrudenick.shared.binding.bindable
import com.lrudenick.shared.data.source.remote.network.EnvironmentProvider
import com.lrudenick.shared.domain.model.MovieDetail
import com.lrudenick.shared.domain.model.genresList
import com.lrudenick.shared.domain.model.releaseYear
import com.lrudenick.shared.domain.use.movieDetail.MovieDetailUse
import com.lrudenick.shared.domain.use.movieDetail.MovieDetailUseFactory
import com.lrudenick.shared.ui.navigation.NavigationViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    movieDetailFactory: MovieDetailUseFactory,
    environmentProvider: EnvironmentProvider
) : MovieDetailUse.Out, MovieDetailController, MovieDetailPresenter() {

    override val coroutineContext: CoroutineContext = viewModelScope.coroutineContext

    override var state: StateEvent by bindable(StateEvent.IDLE, BR.state)

    override val prefixPath: String by bindable(
        environmentProvider.invoke().imgUrlPath,
        BR.prefixPath
    )

    override var movie: MovieDetail? by bindable(null, BR.movie)

    override var year: String by bindable("", BR.year)

    override var rate: String by bindable("", BR.rate)

    override var genres: String by bindable("", BR.genres)

    override var videoKey: String by bindable("", BR.videoKey)

    private val useCase = movieDetailFactory.create(this)

    fun getMovieDetail(movieId: Int) {
        useCase.getMovieDetail(movieId)
        useCase.getVideos(movieId)
    }

    override fun setProcessState(state: MovieDetailUse.MovieDetailState) {
        this.state = when (state) {
            is MovieDetailUse.MovieDetailState.Failed -> StateEvent.ERROR
            MovieDetailUse.MovieDetailState.Idle -> StateEvent.IDLE
            MovieDetailUse.MovieDetailState.Loaded -> StateEvent.LOADED
            MovieDetailUse.MovieDetailState.Loading -> StateEvent.LOADING
        }
    }

    override fun setMovieDetail(movieDetail: MovieDetail) {
        movie = movieDetail

        year = movieDetail.releaseYear()
        rate = String.format("%.1f", movieDetail.rate)
        genres = movieDetail.genresList()
    }

    override fun setVideoId(videoId: String) {
        this.videoKey = videoId
    }

    override fun viewTrailer() {
        if (videoKey.isNotEmpty()) {
            emit(MovieDetailEvents.ViewTrailer(videoKey))
        }
    }


}

abstract class MovieDetailPresenter : NavigationViewModel<MovieDetailEvents>() {
    @get:Bindable
    abstract val state: StateEvent

    @get:Bindable
    abstract val prefixPath: String

    @get:Bindable
    abstract val movie: MovieDetail?

    @get:Bindable
    abstract val year: String

    @get:Bindable
    abstract val rate: String

    @get:Bindable
    abstract val genres: String

    @get:Bindable
    abstract val videoKey: String

}

interface MovieDetailController {
    fun viewTrailer()
}


sealed class MovieDetailEvents {
    class ViewTrailer(val videoKey: String) : MovieDetailEvents()
}
