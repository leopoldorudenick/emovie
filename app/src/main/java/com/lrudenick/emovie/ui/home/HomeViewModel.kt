package com.lrudenick.emovie.ui.home

import androidx.databinding.Bindable
import androidx.lifecycle.viewModelScope
import com.lrudenick.emovie.ui.StateEvent
import com.lrudenick.shared.binding.bindable
import com.lrudenick.shared.data.source.remote.network.EnvironmentProvider
import com.lrudenick.shared.domain.model.Movie
import com.lrudenick.shared.domain.model.MoviesResponse
import com.lrudenick.shared.domain.use.movieList.MovieList
import com.lrudenick.shared.domain.use.movieList.MovieListFactory
import com.lrudenick.shared.ui.navigation.NavigationViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext
import com.lrudenick.emovie.BR
import com.lrudenick.shared.domain.model.MovieFilter

@HiltViewModel
class HomeViewModel @Inject constructor(
    movieListFactory: MovieListFactory,
    environmentProvider: EnvironmentProvider
) : MovieList.Out, HomeController, HomePresenter() {

    override val coroutineContext: CoroutineContext = viewModelScope.coroutineContext

    override var state: StateEvent by bindable(StateEvent.IDLE, BR.state)

    private val useCase = movieListFactory.create(this)

    override var upcomingMoviesList: List<Movie> by bindable(emptyList(), BR.upcomingMoviesList)

    override var topRatedMoviesList: List<Movie> by bindable(emptyList(), BR.topRatedMoviesList)

    override var recommendedMoviesList: List<Movie> by bindable(
        emptyList(),
        BR.recommendedMoviesList
    )

    private var originalRecommendedMovies: List<Movie> = emptyList()

    override val prefixPath: String by bindable(
        environmentProvider.invoke().imgUrlPath,
        BR.prefixPath
    )

    init {
        if (upcomingMoviesList.isEmpty()) {
            useCase.getUpcomingMovies()
        }
        if (topRatedMoviesList.isEmpty()) {
            useCase.getTopRatedMovies()
        }
    }

    override fun setProcessState(state: MovieList.MovieListState) {
        this.state = when (state) {
            is MovieList.MovieListState.Failed -> StateEvent.ERROR
            MovieList.MovieListState.Idle -> StateEvent.IDLE
            MovieList.MovieListState.Loaded -> StateEvent.LOADED
            MovieList.MovieListState.Loading -> StateEvent.LOADING
        }
    }

    override fun setTopRatedMovies(movies: MoviesResponse) {
        topRatedMoviesList = movies.movies
        if (topRatedMoviesList.isNotEmpty()) {
            useCase.getRecommendedMovies(topRatedMoviesList[0].id)
        }
    }

    override fun setUpcomingMovies(movies: MoviesResponse) {
        upcomingMoviesList = movies.movies
    }

    override fun setRecommendedMovies(movies: MoviesResponse) {
        originalRecommendedMovies = movies.movies
        recommendedMoviesList =
            originalRecommendedMovies.sortedByDescending { it.releaseDate }.take(6)
    }

    override fun viewMovieDetail(movieId: Int) {
        emit(HomeEvents.ViewMovieDetail(movieId))
    }

    fun filterUpdated(movieFilter: MovieFilter) {
        if (originalRecommendedMovies.isNotEmpty()) {
            recommendedMoviesList = when (movieFilter) {
                MovieFilter.ALL -> {
                    originalRecommendedMovies.sortedByDescending { it.releaseDate }.take(6)
                }
                MovieFilter.RELEASED_IN_2003 -> {
                    originalRecommendedMovies.filter { it.releaseDate.startsWith("2003") }
                        .sortedByDescending { it.releaseDate }.take(6)
                }
                MovieFilter.IN_ENGLISH -> {
                    originalRecommendedMovies.filter { it.language == "en" }
                        .sortedByDescending { it.releaseDate }.take(6)
                }
                MovieFilter.IN_HINDI -> {
                    originalRecommendedMovies.filter { it.language == "hi" }
                        .sortedByDescending { it.releaseDate }.take(6)
                }
            }
        }
    }

}

abstract class HomePresenter : NavigationViewModel<HomeEvents>() {
    @get:Bindable
    abstract val upcomingMoviesList: List<Movie>

    @get:Bindable
    abstract val topRatedMoviesList: List<Movie>

    @get:Bindable
    abstract val recommendedMoviesList: List<Movie>

    @get:Bindable
    abstract val state: StateEvent

    @get:Bindable
    abstract val prefixPath: String
}

interface HomeController {
    fun viewMovieDetail(movieId: Int)
}


sealed class HomeEvents {
    class ViewMovieDetail(val movieId: Int) : HomeEvents()
}
