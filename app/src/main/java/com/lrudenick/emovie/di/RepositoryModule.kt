package com.lrudenick.emovie.di

import com.lrudenick.shared.data.MovieRepositoryImpl
import com.lrudenick.shared.data.source.local.db.AppDatabase
import com.lrudenick.shared.data.source.remote.network.service.ApiService
import com.lrudenick.shared.domain.repository.MovieRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Hilt module (see [Module]) for application scoped (see [SingletonComponent])
 * dependency injection.
 * Provide instances of the different repositories in the app
 */

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideMovieRepository(
        apiService: ApiService,
        appDatabase: AppDatabase
    ): MovieRepository =
        MovieRepositoryImpl(apiService, appDatabase)

}
