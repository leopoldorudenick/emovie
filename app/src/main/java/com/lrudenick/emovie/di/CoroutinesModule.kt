package com.lrudenick.emovie.di

import com.lrudenick.shared.di.DefaultDispatcher
import com.lrudenick.shared.di.IoDispatcher
import com.lrudenick.shared.di.MainDispatcher
import com.lrudenick.shared.di.MainImmediateDispatcher
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

/**
 * Hilt module (see [Module]) for application scoped (see [SingletonComponent])
 * dependency injection.
 * Used to provide [CoroutineDispatcher]s to inject according to the best practices
 * described here:
 * [https://developer.android.com/kotlin/coroutines/coroutines-best-practices#inject-dispatchers]
 */

@InstallIn(SingletonComponent::class)
@Module
object CoroutinesModule {

    @DefaultDispatcher
    @Provides
    fun providesDefaultDispatcher(): CoroutineDispatcher = Dispatchers.Default

    @IoDispatcher
    @Provides
    fun providesIoDispatcher(): CoroutineDispatcher = Dispatchers.IO

    @MainDispatcher
    @Provides
    fun providesMainDispatcher(): CoroutineDispatcher = Dispatchers.Main

    @MainImmediateDispatcher
    @Provides
    fun providesMainImmediateDispatcher(): CoroutineDispatcher = Dispatchers.Main.immediate
}
