package com.lrudenick.emovie.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.lrudenick.shared.BuildConfig
import com.lrudenick.shared.data.source.remote.network.*
import com.lrudenick.shared.data.source.remote.network.service.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import okhttp3.HttpUrl
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Hilt module (see [Module]) for application scoped (see [SingletonComponent])
 * dependency injection.
 */

@Module
@InstallIn(SingletonComponent::class)
object ApiModule {

    /**
     * Provides an instance of [Json], for kotlinx-serialization
     */
    @ExperimentalSerializationApi
    @Provides
    @Singleton
    fun provideJson(): Json {
        return Json {
            ignoreUnknownKeys = true
            coerceInputValues = true
            explicitNulls = false
            encodeDefaults = true
        }
    }

    /**
     * Provides an instance of [Converter.Factory], for kotlinx-serialization
     */
    @ExperimentalSerializationApi
    @Provides
    @Singleton
    fun provideKotlinxSerializationConverterFactory(json: Json): Converter.Factory {
        val contentType = "application/json".toMediaType()
        return json.asConverterFactory(contentType)
    }

    /**
     * Provides an instance of [ApiKeyInterceptor], used to add headers (like the
     * Authorization Header) to HTTP requests
     */
    @Provides
    @Singleton
    fun provideAuthHeaderInterceptor(
        environmentProvider: EnvironmentProvider
    ): ApiKeyInterceptor {
        return ApiKeyInterceptor(environmentProvider.invoke())
    }

    /**
     * Provides an instance of [HttpLoggingInterceptor], which logs request and response
     * information. If [BuildConfig.DEBUG], It will log request and response lines and
     * their respective headers and bodies (if present. Level [HttpLoggingInterceptor.Level.BODY]).
     * Otherwise it will log only request and response lines
     * (level [HttpLoggingInterceptor.Level.BASIC])
     */
    @Provides
    @Singleton
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.BASIC)
        }
    }

    /**
     * Provides an instance of [UserAgentInterceptor], used to add
     * used to add the "User-Agent" header to HTTP requests, with
     * data about the app name and platform.
     * E.g. `User-Agent: EMovie App (30; Android SDK built for x86)`
     */
    @Provides
    @Singleton
    fun provideUserAgentInterceptor(): UserAgentInterceptor =
        UserAgentInterceptor("EMovie App")

    @Provides
    @Singleton
    fun provideOkHttpClient(
        httpLoggingInterceptor: HttpLoggingInterceptor,
        userAgentInterceptor: UserAgentInterceptor,
        apiKeyInterceptor: ApiKeyInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder().apply {
            connectTimeout(30, TimeUnit.SECONDS)
            readTimeout(30, TimeUnit.SECONDS)
            writeTimeout(30, TimeUnit.SECONDS)
            addInterceptor(TimeoutInterceptor())
            addInterceptor(apiKeyInterceptor)
            addInterceptor(userAgentInterceptor)
            addInterceptor(httpLoggingInterceptor)
        }.build()
    }

    /**
     * Provides an instance of [Retrofit] for use with [ApiService]
     */
    @Provides
    @Singleton
    fun provideRetrofit(
        httpClient: OkHttpClient,
        kotlinxConverterFactory: Converter.Factory,
        environmentProvider: EnvironmentProvider
    ): Retrofit {
        return buildRetrofitByHost(
            environmentProvider.invoke().apiUrl,
            httpClient.newBuilder().build(),
            kotlinxConverterFactory
        )
    }

    /**
     * Creates a new instance of [Retrofit] with [httpUrl] as `baseUrl`, [httpClient]
     * as `client` and [kotlinxConverterFactory] to create a [Converter.Factory]
     */
    private fun buildRetrofitByHost(
        httpUrl: HttpUrl,
        httpClient: OkHttpClient,
        kotlinxConverterFactory: Converter.Factory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(httpUrl)
            .client(httpClient)
            .addConverterFactory(kotlinxConverterFactory)
            .addCallAdapterFactory(ResultCallAdapterFactory())
            .build()
    }

    /**
     * Provides an implementation of the API endpoints defined by the service interface [ApiService]
     */
    @Provides
    @Singleton
    fun provideApiService(
        retrofit: Retrofit
    ): ApiService {
        return retrofit.create(ApiService::class.java)
    }

}
