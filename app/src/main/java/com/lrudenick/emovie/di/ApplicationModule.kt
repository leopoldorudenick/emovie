package com.lrudenick.emovie.di

import android.content.Context
import com.lrudenick.shared.data.source.local.db.AppDatabase
import com.lrudenick.shared.data.source.remote.network.EnvironmentProvider
import com.lrudenick.shared.data.source.remote.network.ProdEnvironmentProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Hilt module (see [Module]) for application scoped (see [SingletonComponent])
 * dependency injection.
 */

@Module
@InstallIn(SingletonComponent::class)
object ApplicationModule {

    /**
     * Provides an instance of [EnvironmentProvider], using [ProdEnvironmentProvider]
     * implementation, which holds all the url routes for the production environment
     */
    @Provides
    fun providesEnvironmentProvider(): EnvironmentProvider = ProdEnvironmentProvider()


    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return AppDatabase.buildDatabase(context)
    }

}
