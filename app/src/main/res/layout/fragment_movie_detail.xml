<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <import type="com.lrudenick.emovie.ui.StateEvent" />

        <variable
            name="presenter"
            type="com.lrudenick.emovie.ui.movieDetail.MovieDetailPresenter" />

        <variable
            name="controller"
            type="com.lrudenick.emovie.ui.movieDetail.MovieDetailController" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <ImageView
            android:id="@+id/poster_img"
            imageUrl="@{presenter.movie.posterImg}"
            prefixPath="@{presenter.prefixPath}"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:contentDescription="@string/iv_content_description"
            android:scaleType="centerCrop"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

        <androidx.appcompat.widget.Toolbar
            android:id="@+id/toolbar"
            android:layout_width="match_parent"
            android:layout_height="?attr/actionBarSize"
            android:background="@color/transparent_dark_grey"
            android:elevation="10dp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:navigationIcon="@drawable/ic_back"
            app:popupTheme="@style/Theme.EMovie.PopupOverlay" />

        <androidx.core.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:background="@color/transparent_dark_grey"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/toolbar">

            <androidx.constraintlayout.widget.ConstraintLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:background="@android:color/transparent">

                <androidx.constraintlayout.widget.ConstraintLayout
                    style="@style/GeneralContent"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:paddingBottom="@dimen/margin_bottom"
                    android:visibility="@{presenter.movie == null ? View.GONE : View.VISIBLE, default=gone}"
                    app:layout_constraintTop_toTopOf="parent">

                    <TextView
                        android:id="@+id/movie_title"
                        style="@style/Title"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/movie_title_margin_top"
                        android:text="@{presenter.movie.title}"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toTopOf="parent"
                        tools:text="Her" />

                    <TextView
                        android:id="@+id/year"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/list_margin_top"
                        android:layout_marginEnd="@dimen/text_margin"
                        android:background="@drawable/text_back"
                        android:text="@{presenter.year}"
                        app:layout_constraintEnd_toStartOf="@+id/language"
                        app:layout_constraintHorizontal_chainStyle="packed"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@+id/movie_title"
                        tools:text="2013" />

                    <TextView
                        android:id="@+id/language"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/text_margin"
                        android:background="@drawable/text_back"
                        android:text="@{presenter.movie.language}"
                        app:layout_constraintEnd_toStartOf="@+id/rate"
                        app:layout_constraintStart_toEndOf="@+id/year"
                        app:layout_constraintTop_toTopOf="@+id/year"
                        tools:text="en" />

                    <TextView
                        android:id="@+id/rate"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:background="@drawable/rate_back"
                        android:text="@{presenter.rate}"
                        android:textStyle="bold"
                        app:drawableStartCompat="@drawable/ic_star_rate"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toEndOf="@+id/language"
                        app:layout_constraintTop_toTopOf="@+id/year"
                        tools:text="8.5" />

                    <TextView
                        android:id="@+id/genres"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/genres_margin_top"
                        android:text="@{presenter.genres}"
                        android:textColor="@color/white"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@id/rate"
                        tools:text="Heartfelt * Romance * Sci-fi * Drama" />

                    <androidx.appcompat.widget.AppCompatButton
                        android:id="@+id/btn_see_trailer"
                        click="@{controller::viewTrailer}"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/text_margin"
                        android:background="@drawable/transparent_button_back"
                        android:text="@string/btn_see_trailer"
                        android:textAllCaps="false"
                        android:textColor="@color/white"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@+id/genres" />

                    <TextView
                        android:id="@+id/movie_plot_title"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/genres_margin_top"
                        android:text="@string/movie_plot_title"
                        android:textColor="@color/white"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@id/btn_see_trailer" />

                    <TextView
                        android:id="@+id/movie_plot_desc"
                        android:layout_width="0dp"
                        android:layout_height="wrap_content"
                        android:layout_marginTop="@dimen/text_margin"
                        android:text="@{presenter.movie.plot}"
                        android:textColor="@color/white"
                        app:layout_constraintEnd_toEndOf="parent"
                        app:layout_constraintStart_toStartOf="parent"
                        app:layout_constraintTop_toBottomOf="@id/movie_plot_title"
                        tools:text="In a near future, a lonely writer develops an unlikely relationship with an operating system designed to meet his every need." />

                </androidx.constraintlayout.widget.ConstraintLayout>
            </androidx.constraintlayout.widget.ConstraintLayout>

        </androidx.core.widget.NestedScrollView>

        <ProgressBar
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:visibility="@{presenter.movie == null ? View.VISIBLE : View.GONE, default=visible}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />

    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
