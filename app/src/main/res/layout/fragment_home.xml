<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <import type="android.view.View" />

        <import type="com.lrudenick.emovie.ui.StateEvent" />

        <variable
            name="presenter"
            type="com.lrudenick.emovie.ui.home.HomePresenter" />

        <variable
            name="controller"
            type="com.lrudenick.emovie.ui.home.HomeController" />
    </data>

    <androidx.core.widget.NestedScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <androidx.constraintlayout.widget.ConstraintLayout
            style="@style/InnerContainer"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/black">

            <ImageView
                android:id="@+id/app_icon"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/icon_margin_top"
                android:layout_marginEnd="@dimen/fragment_horizontal_margin"
                android:contentDescription="@string/iv_content_description"
                android:src="@drawable/ic_emovie"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />

            <TextView
                android:id="@+id/upcoming_title"
                style="@style/H5"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/title_margin_top"
                android:text="@string/upcoming_movies_title"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/app_icon" />

            <com.lrudenick.emovie.ui.home.MoviesRecyclerView
                android:id="@+id/upcoming_list"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/list_margin_top"
                android:minHeight="200dp"
                android:orientation="horizontal"
                android:visibility="@{presenter.upcomingMoviesList.size() != 0 ? View.VISIBLE : View.INVISIBLE, default=invisible}"
                app:layoutManager="androidx.recyclerview.widget.LinearLayoutManager"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/upcoming_title"
                app:movieList="@{presenter.upcomingMoviesList}"
                app:movieSelector="@{controller::viewMovieDetail}"
                app:prefixPath="@{presenter.prefixPath}"
                tools:listitem="@layout/item_movie" />

            <ProgressBar
                android:id="@+id/upcoming_loading"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="@dimen/fragment_horizontal_margin"
                android:visibility="@{presenter.upcomingMoviesList.size() == 0 ? View.VISIBLE : View.GONE, default=visible}"
                app:layout_constraintBottom_toBottomOf="@+id/upcoming_list"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="@+id/upcoming_list" />

            <TextView
                android:id="@+id/top_rated_title"
                style="@style/H5"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/title_margin_top"
                android:text="@string/top_rated_title"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/upcoming_list" />

            <com.lrudenick.emovie.ui.home.MoviesRecyclerView
                android:id="@+id/top_rated_list"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/list_margin_top"
                android:clipToPadding="false"
                android:minHeight="200dp"
                android:orientation="horizontal"
                android:visibility="@{presenter.topRatedMoviesList.size() != 0 ? View.VISIBLE : View.INVISIBLE, default=invisible}"
                app:layoutManager="androidx.recyclerview.widget.LinearLayoutManager"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/top_rated_title"
                app:movieList="@{presenter.topRatedMoviesList}"
                app:movieSelector="@{controller::viewMovieDetail}"
                app:prefixPath="@{presenter.prefixPath}"
                tools:listitem="@layout/item_movie" />

            <ProgressBar
                android:id="@+id/top_rated_loading"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="@dimen/fragment_horizontal_margin"
                android:visibility="@{presenter.topRatedMoviesList.size() == 0 ? View.VISIBLE : View.GONE, default=visible}"
                app:layout_constraintBottom_toBottomOf="@+id/top_rated_list"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="@+id/top_rated_list" />

            <TextView
                android:id="@+id/recommendations_title"
                style="@style/H5"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/title_margin_top"
                android:text="@string/recommendations_title"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/top_rated_list" />

            <HorizontalScrollView
                android:id="@+id/filter_buttons"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/filter_margin_top"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/recommendations_title">

                <RadioGroup
                    android:id="@+id/filters_group"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <RadioButton
                        android:id="@+id/all"
                        style="@style/BtnFilter"
                        android:text="@string/all_filter" />

                    <RadioButton
                        android:id="@+id/english"
                        style="@style/BtnFilter"
                        android:text="@string/in_english_filter" />

                    <RadioButton
                        android:id="@+id/hindi"
                        style="@style/BtnFilter"
                        android:text="@string/in_hindi_filter" />

                    <RadioButton
                        android:id="@+id/year2003"
                        style="@style/BtnFilter"
                        android:text="@string/released_in_2003" />
                </RadioGroup>
            </HorizontalScrollView>

            <com.lrudenick.emovie.ui.home.MoviesRecyclerView
                android:id="@+id/recommendations_list"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/list_margin_top"
                android:layout_marginEnd="@dimen/fragment_horizontal_margin"
                android:minHeight="200dp"
                android:orientation="vertical"
                android:visibility="@{presenter.recommendedMoviesList.size() != 0 ? View.VISIBLE : View.INVISIBLE, default=invisible}"
                app:layoutManager="androidx.recyclerview.widget.GridLayoutManager"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toBottomOf="@+id/filter_buttons"
                app:movieList="@{presenter.recommendedMoviesList}"
                app:movieSelector="@{controller::viewMovieDetail}"
                app:prefixPath="@{presenter.prefixPath}"
                app:spanCount="2"
                tools:listitem="@layout/item_movie" />

            <ProgressBar
                android:id="@+id/recommendations_loading"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginEnd="@dimen/fragment_horizontal_margin"
                android:visibility="@{presenter.recommendedMoviesList.size() == 0 ? View.VISIBLE : View.GONE, default=visible}"
                app:layout_constraintBottom_toBottomOf="@+id/recommendations_list"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="@+id/recommendations_list" />

        </androidx.constraintlayout.widget.ConstraintLayout>

    </androidx.core.widget.NestedScrollView>
</layout>
