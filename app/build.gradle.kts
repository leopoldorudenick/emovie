plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize")
    id("kotlinx-serialization")
    id("dagger.hilt.android.plugin")
    id("androidx.navigation.safeargs.kotlin")
    id("com.vanniktech.android.junit.jacoco")
}

android {
    compileSdk = AndroidSdk.compile
    defaultConfig {
        applicationId = "com.lrudenick.emovie"
        minSdk = AndroidSdk.min
        targetSdk = AndroidSdk.target
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        vectorDrawables.useSupportLibrary = true
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    buildFeatures {
        viewBinding = true
        dataBinding = true
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(project(":shared"))
    with(Deps.Material) {
        implementation(material)
    }
    with(Deps.Androidx) {
        implementation(appcompat)
        implementation(constraintLayout)
        implementation(lifecycleLivedata)
        implementation(lifecycleViewmodel)
        implementation(navigationFragment)
        implementation(navigationUi)
        testImplementation(coreTesting)
    }
    with(Deps.Dagger) {
        implementation(hiltAndroid)
        kapt(hiltAndroidCompiler)
    }
    with(Deps.Glide) {
        implementation(glide)
        annotationProcessor(glideCompiler)
    }
    with(Deps.Kotlinx) {
        implementation(coroutinesCore)
        implementation(serializationCore)
        implementation(serializationRuntime)
        testImplementation(serializationRuntime)
        testImplementation(coroutinesTest)
    }
    with(Deps.Legacy) {
        implementation(legacySupport)
    }
    with(Deps.Retrofit) {
        implementation(retrofitRuntime)
        implementation(retrofit2Converter)
    }
    with(Deps.OkHttp) {
        implementation(okHttpRuntime)
        implementation(okHttpLoggingInterceptor)
    }
    with(Deps.Room) {
        implementation(roomKtx)
        implementation(roomRuntime)
        kapt(roomCompiler)
        testImplementation(roomKtx)
        testImplementation(roomRuntime)
    }
    with(Deps.Timber) {
        implementation(timberLogging)
    }
    with(Deps.Junit) {
        testImplementation(junit)
    }
}

// Allow references to generated code
kapt {
    correctErrorTypes = true
}

// Jacoco reporting:
junitJacoco {
    jacocoVersion = "0.8.7"
    includeNoLocationClasses = false
    excludes = listOf(
        // Databinding
        "**/DataBinderMapperImpl*.*",
        "**/DataBindingInfo*.*",
        "**/MobileNavigationDirections*.*",
        "android/databinding/**/*.class",
        "**/android/databinding/*Binding.class",
        "**/BR.*",
        "**/databinding/**",

        // Android
        "**/R.class",
        "**/R\$*.class",
        "**/BuildConfig.*",
        "**/Manifest*.*",
        "**/android/**",

        // Hilt
        "**/*_MembersInjector.class",
        "**/*_Factory.*",
        "**/*InstanceHolder*",
        "**/*ModuleDeps*",
        "**/*_AssistedFactory.*",
        "**/*Hilt*",
        "**/**ModuleDeps",
        "**/di/**",

        // JNI:
        "**/jni/**",

        // extensions
        "**/extensions/**",
    )
}

