package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.types.Converter
import com.lrudenick.shared.types.mapInbound
import com.lrudenick.shared.types.mapOutbound


class ResultConverter<T, F>(private val converter: Converter<T, F>) :
    Converter<Result<T>, Result<F>> {

    override fun convertInbound(value: Result<T>): Result<F> {
        return value.fold({
            Result.success(converter.convertInbound(it))
        }, {
            Result.failure(it)
        })
    }

    override fun convertOutbound(value: Result<F>): Result<T> {
        return value.fold({
            Result.success(converter.convertOutbound(it))
        }, {
            Result.failure(it)
        })
    }

    fun mapInbound(value: Result<List<T>>): Result<List<F>> {
        return value.fold({
            Result.success(converter.mapInbound(it))
        }, {
            Result.failure(it)
        })
    }

    fun mapOutbound(value: Result<List<F>>): Result<List<T>> {
        return value.fold({
            Result.success(converter.mapOutbound(it))
        }, {
            Result.failure(it)
        })
    }
}
