package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.data.source.remote.model.CloudVideosResponse
import com.lrudenick.shared.domain.model.VideosResponse
import com.lrudenick.shared.types.Converter
import com.lrudenick.shared.types.mapInbound
import com.lrudenick.shared.types.mapOutbound

class CloudVideoResponseConverter : Converter<VideosResponse, CloudVideosResponse> {

    private val cloudVideoConverter = CloudVideoConverter()

    override fun convertInbound(value: VideosResponse): CloudVideosResponse {
        return CloudVideosResponse(
            id = value.id,
            videos = cloudVideoConverter.mapInbound(value.videos)
        )
    }

    override fun convertOutbound(value: CloudVideosResponse): VideosResponse {
        return VideosResponse(
            id = value.id,
            videos = cloudVideoConverter.mapOutbound(value.videos)
        )
    }
}
