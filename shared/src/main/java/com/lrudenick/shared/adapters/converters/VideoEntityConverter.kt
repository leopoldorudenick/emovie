package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.data.source.local.entity.VideoEntity
import com.lrudenick.shared.domain.model.Video
import com.lrudenick.shared.types.Converter

class VideoEntityConverter : Converter<Video, VideoEntity> {

    override fun convertOutbound(value: VideoEntity): Video {
        return Video(
            id = value.videoId,
            key = value.key,
            official = value.official,
            site = value.site,
            type = value.type
        )
    }

    override fun convertInbound(value: Video): VideoEntity {
        return VideoEntity(
            videoId = value.id,
            key = value.key,
            official = value.official,
            site = value.site,
            type = value.type
        )
    }
}
