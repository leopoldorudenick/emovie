package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.data.source.local.entity.MovieDetailEntity
import com.lrudenick.shared.domain.model.MovieDetail
import com.lrudenick.shared.types.Converter

class MovieDetailEntityConverter : Converter<MovieDetail, MovieDetailEntity> {
    override fun convertOutbound(value: MovieDetailEntity): MovieDetail {
        return MovieDetail(
            id = value.movieId,
            title = value.title,
            adult = value.adult,
            plot = value.plot,
            language = value.language,
            posterImg = value.posterImg,
            rate = value.rate,
            releaseDate = value.releaseDate,
            genres = value.genres
        )
    }

    override fun convertInbound(value: MovieDetail): MovieDetailEntity {
        return MovieDetailEntity(
            movieId = value.id,
            title = value.title,
            adult = value.adult,
            plot = value.plot,
            language = value.language,
            posterImg = value.posterImg,
            rate = value.rate,
            releaseDate = value.releaseDate,
            genres = value.genres
        )
    }
}
