package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.data.source.local.entity.MovieEntity
import com.lrudenick.shared.domain.model.Movie
import com.lrudenick.shared.types.Converter

class MovieEntityConverter : Converter<Movie, MovieEntity> {

    override fun convertOutbound(value: MovieEntity): Movie {
        return Movie(
            id = value.movieId,
            title = value.title,
            adult = value.adult,
            plot = value.plot,
            language = value.language,
            posterImg = value.posterImg,
            rate = value.rate,
            releaseDate = value.releaseDate
        )
    }

    override fun convertInbound(value: Movie): MovieEntity {
        return MovieEntity(
            movieId = value.id,
            title = value.title,
            adult = value.adult,
            plot = value.plot,
            language = value.language,
            posterImg = value.posterImg,
            rate = value.rate,
            releaseDate = value.releaseDate
        )
    }
}
