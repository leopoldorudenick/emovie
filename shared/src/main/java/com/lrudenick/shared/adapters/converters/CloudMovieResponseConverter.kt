package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.data.source.remote.model.CloudMovieResponse
import com.lrudenick.shared.domain.model.MoviesResponse
import com.lrudenick.shared.types.Converter
import com.lrudenick.shared.types.mapInbound
import com.lrudenick.shared.types.mapOutbound

class CloudMovieResponseConverter : Converter<MoviesResponse, CloudMovieResponse> {

    private val cloudMovieConverter = CloudMovieConverter()

    override fun convertInbound(value: MoviesResponse): CloudMovieResponse {
        return CloudMovieResponse(
            page = value.page,
            results = cloudMovieConverter.mapInbound(value.movies)
        )
    }

    override fun convertOutbound(value: CloudMovieResponse): MoviesResponse {
        return MoviesResponse(
            page = value.page,
            movies = cloudMovieConverter.mapOutbound(value.results)
        )
    }
}
