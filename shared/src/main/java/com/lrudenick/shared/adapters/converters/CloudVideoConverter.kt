package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.data.source.remote.model.CloudVideo
import com.lrudenick.shared.domain.model.Video
import com.lrudenick.shared.types.Converter

class CloudVideoConverter : Converter<Video, CloudVideo> {

    override fun convertInbound(value: Video): CloudVideo {
        return CloudVideo(
            id = value.id,
            key = value.key,
            official = value.official,
            site = value.site,
            type = value.type
        )
    }

    override fun convertOutbound(value: CloudVideo): Video {
        return Video(
            id = value.id,
            key = value.key,
            official = value.official,
            site = value.site,
            type = value.type
        )
    }

}
