package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.data.source.remote.model.CloudMovieDetail
import com.lrudenick.shared.domain.model.MovieDetail
import com.lrudenick.shared.types.Converter

class CloudMovieDetailConverter : Converter<MovieDetail, CloudMovieDetail> {

    override fun convertInbound(value: MovieDetail): CloudMovieDetail {
        return CloudMovieDetail(
            id = value.id,
            title = value.title,
            adult = value.adult,
            plot = value.plot,
            language = value.language,
            posterImg = value.posterImg,
            rate = value.rate,
            releaseDate = value.releaseDate,
            genres = emptyList()
        )
    }

    override fun convertOutbound(value: CloudMovieDetail): MovieDetail {
        return MovieDetail(
            id = value.id,
            title = value.title,
            adult = value.adult,
            plot = value.plot,
            language = value.language,
            posterImg = value.posterImg,
            rate = value.rate,
            releaseDate = value.releaseDate,
            genres = value.genres.joinToString(separator = "-") {
                it.name
            }
        )
    }

}
