package com.lrudenick.shared.adapters.converters

import com.lrudenick.shared.data.source.remote.model.CloudMovie
import com.lrudenick.shared.domain.model.Movie
import com.lrudenick.shared.types.Converter

class CloudMovieConverter : Converter<Movie, CloudMovie> {

    override fun convertInbound(value: Movie): CloudMovie {
        return CloudMovie(
            id = value.id,
            title = value.title,
            adult = value.adult,
            plot = value.plot,
            language = value.language,
            posterImg = value.posterImg,
            rate = value.rate,
            releaseDate = value.releaseDate
        )
    }

    override fun convertOutbound(value: CloudMovie): Movie {
        return Movie(
            id = value.id,
            title = value.title,
            adult = value.adult,
            plot = value.plot,
            language = value.language,
            posterImg = value.posterImg,
            rate = value.rate,
            releaseDate = value.releaseDate
        )
    }

}
