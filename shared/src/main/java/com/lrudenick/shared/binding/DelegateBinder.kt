package com.lrudenick.shared.binding

import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * A class for delegating fields of [ObservableViewModel] to notify bound views on property
 * change. Ideally, these are things like [Int], [Boolean], [String], etc. for directly
 * constructing the view. In this case, updating value will trigger the correct view update.
 * In the case that you're setting a [List] or similar, note that the [notifyPropertyChanged] call
 * only triggers on [setValue]. This means that in-place updates will not be reflected on the view.
 * You can either (a) use an Observable list type that emits the correct notify property change
 * events to the view model, or (b) call [set] with the list update. (a) is preferred, but (b) is
 * simpler.
 *
 * For example:
 * ```
 * @get:Bindable
 * var shouldShowCoolButton by bindable(true, BR.shouldShowCoolButton)
 * ```
 *
 * Implementation note: It would probably be possible to do the notify property change
 * lookup via reflection, and only pass the static BR member in, but in practice, taking
 * the kotlin-reflection lib as a dependency seems less ideal that simply passing the fully
 * qualified BR-int.
 *
 */
class DelegateBinder<T>(private var default: T,
                        private vararg val notify: Int) : ReadWriteProperty<Any?, T> {

    override fun getValue(thisRef: Any?, property: KProperty<*>): T = default

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.default = value
        (thisRef as? ObservableViewModel)?.run {
            notify.forEach { notifyPropertyChanged(it) }
        }
    }
}

// Vanity constructor
fun <T> bindable(default: T, vararg notify: Int) = DelegateBinder(default, *notify)
