package com.lrudenick.shared.data.source.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CloudGenre(
    @SerialName("id") val id: Int,
    @SerialName("name") val name: String
)
