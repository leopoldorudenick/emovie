package com.lrudenick.shared.data.source.remote.network

import android.os.Build
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Set the user agent for the app.
 */
class UserAgentInterceptor(
    private val appName: String,
    private var platformVariant: String? = null
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val head = appName
        val tail = if (platformVariant != null) {
            "(${platformVariant}; ${Build.VERSION.SDK_INT}; ${Build.MODEL})"
        } else {
            "(${Build.VERSION.SDK_INT}; ${Build.MODEL})"
        }

        return chain.proceed(
            chain.request().newBuilder()
                .header("User-Agent", "$head $tail")
                .build()
        )
    }
}

