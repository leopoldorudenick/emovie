package com.lrudenick.shared.data.source.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.lrudenick.shared.domain.model.MovieDetail

/**
 * This class represents [MovieDetail] data.
 *
 * The [ColumnInfo] name is explicitly declared to allow flexibility for renaming the data class
 * properties without requiring changing the column name.
 */
@Entity(tableName = "movieDetail")
data class MovieDetailEntity(
    @PrimaryKey
    @ColumnInfo(name = "movieId")
    val movieId: Int,

    @ColumnInfo(name = "title")
    val title: String,

    @ColumnInfo(name = "adult")
    val adult: Boolean,

    @ColumnInfo(name = "plot")
    val plot: String,

    @ColumnInfo(name = "posterImg")
    val posterImg: String? = null,

    @ColumnInfo(name = "language")
    val language: String,

    @ColumnInfo(name = "rate")
    val rate: Double,

    @ColumnInfo(name = "releaseDate")
    val releaseDate: String,

    @ColumnInfo(name = "genres")
    val genres: String
)
