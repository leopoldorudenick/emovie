package com.lrudenick.shared.data.source.local.entity

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * The Data Access Object for the [MovieEntity] class.
 */
@Dao
interface MovieDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movies: List<MovieEntity>)

    @Query("SELECT *, movieId FROM movie")
    fun getAllMovies(): List<MovieEntity>

    @Query("SELECT *, movieId FROM movie WHERE movie.rate >= 8.5")
    fun getTopRatedMovies(): List<MovieEntity>

    @Query("SELECT *, movieId FROM movie WHERE movie.upcoming = 1")
    fun getUpcomingMovies(): List<MovieEntity>

    @Query("SELECT *, movieId FROM movie WHERE movie.recommended = 1 ORDER BY movie.releaseDate DESC")
    fun getRecommendedMovies(): List<MovieEntity>
}
