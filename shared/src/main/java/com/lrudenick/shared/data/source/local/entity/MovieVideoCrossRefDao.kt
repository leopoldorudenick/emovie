package com.lrudenick.shared.data.source.local.entity

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * The Data Access Object for the [MovieVideoCrossRefEntity] class.
 */
@Dao
interface MovieVideoCrossRefDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movieVideo: MovieVideoCrossRefEntity)

    @Query("SELECT *, movieId, videoId FROM movieVideoCrossRef WHERE movieId = :movieId AND videoId = :videoId")
    fun getVideoCrossRef(movieId: Int, videoId: String): MovieVideoCrossRefEntity
}
