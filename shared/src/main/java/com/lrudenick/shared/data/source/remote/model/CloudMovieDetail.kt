package com.lrudenick.shared.data.source.remote.model

import com.lrudenick.shared.domain.model.Genre
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CloudMovieDetail(
    @SerialName("id") val id: Int,
    @SerialName("title") val title: String,
    @SerialName("adult") val adult: Boolean,
    @SerialName("overview") val plot: String,
    @SerialName("poster_path") val posterImg: String? = null,
    @SerialName("original_language") val language: String,
    @SerialName("vote_average") val rate: Double,
    @SerialName("release_date") val releaseDate: String,
    @SerialName("genres") val genres: List<CloudGenre>
)
