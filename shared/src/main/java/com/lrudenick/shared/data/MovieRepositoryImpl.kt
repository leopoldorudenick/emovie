package com.lrudenick.shared.data

import androidx.annotation.WorkerThread
import com.lrudenick.shared.adapters.converters.*
import com.lrudenick.shared.data.source.local.db.AppDatabase
import com.lrudenick.shared.data.source.local.entity.MovieEntity
import com.lrudenick.shared.data.source.remote.network.service.ApiService
import com.lrudenick.shared.domain.model.MovieDetail
import com.lrudenick.shared.domain.model.MoviesResponse
import com.lrudenick.shared.domain.model.VideosResponse
import com.lrudenick.shared.domain.repository.MovieRepository
import com.lrudenick.shared.types.mapInbound
import com.lrudenick.shared.types.mapOutbound
import timber.log.Timber
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val api: ApiService,
    private val appDatabase: AppDatabase
) : MovieRepository {

    private val cloudMovieResponseConverter = CloudMovieResponseConverter()
    private val movieConverter = MovieEntityConverter()
    private val movieDetailEntityConverter = MovieDetailEntityConverter()
    private val cloudMovieDetailConverter = CloudMovieDetailConverter()
    private val cloudVideoResponseConverter = CloudVideoResponseConverter()

    @WorkerThread
    override suspend fun getUpcomingMovies(): Result<MoviesResponse> {
        val response: MoviesResponse? = try {
            cloudMovieResponseConverter.convertOutbound(api.getUpcomingMovies())
        } catch (e: Exception) {
            val savedMovies = appDatabase.movieDao().getUpcomingMovies()
            if (savedMovies.isNotEmpty()) {
                createMovieResponse(savedMovies)
            } else {
                null
            }
        }

        return if (response == null) {
            Result.failure(Exception("Remote returned no data"))
        } else {
            // Save data to db
            appDatabase.movieDao()
                .insertAll(movieConverter.mapInbound(response.movies).map {
                    it.upcoming = true
                    it
                })
            Result.success(response)
        }
    }

    @WorkerThread
    override suspend fun getTopRatedMovies(): Result<MoviesResponse> {
        val response: MoviesResponse? = try {
            cloudMovieResponseConverter.convertOutbound(api.getTopRatedMovies())
        } catch (e: Exception) {
            val savedMovies = appDatabase.movieDao().getTopRatedMovies()
            if (savedMovies.isNotEmpty()) {
                createMovieResponse(savedMovies)
            } else {
                null
            }
        }

        return if (response == null) {
            Result.failure(Exception("Remote returned no data"))
        } else {
            // Save data to db
            appDatabase.movieDao()
                .insertAll(movieConverter.mapInbound(response.movies))
            Result.success(response)
        }
    }

    override suspend fun getRecommendations(movieId: Int): Result<MoviesResponse> {
        val response: MoviesResponse? = try {
            cloudMovieResponseConverter.convertOutbound(api.getRecommendations(movieId))
        } catch (e: Exception) {
            val savedMovies = appDatabase.movieDao().getRecommendedMovies()
            if (savedMovies.isNotEmpty()) {
                createMovieResponse(savedMovies)
            } else {
                null
            }
        }

        return if (response == null) {
            Result.failure(Exception("Remote returned no data"))
        } else {
            // Save data to db
            appDatabase.movieDao()
                .insertAll(movieConverter.mapInbound(response.movies).map {
                    it.recommended = true
                    it
                })
            Result.success(response)
        }
    }

    @WorkerThread
    override suspend fun getMovieDetail(movieId: Int): Result<MovieDetail> {
        val response: MovieDetail? = try {
            cloudMovieDetailConverter.convertOutbound(api.getMovieDetail(movieId))
        } catch (e: Exception) {
            movieDetailEntityConverter.convertOutbound(
                appDatabase.movieDetailDao().getMovieDetail(movieId)
            )
        }

        return if (response == null) {
            Result.failure(Exception("Remote returned no data"))
        } else {
            // Save data to db
            appDatabase.movieDetailDao()
                .insert(movieDetailEntityConverter.convertInbound(response))
            Result.success(response)
        }
    }

    @WorkerThread
    override suspend fun getVideos(movieId: Int): Result<VideosResponse> {
        return try {
            val response = api.getMovieVideos(movieId)
            Result.success(cloudVideoResponseConverter.convertOutbound(response))
        } catch (e: Exception) {
            Timber.e(e)
            Result.failure(Exception("Remote returned no data"))
        }
    }

    private fun createMovieResponse(list: List<MovieEntity>) = MoviesResponse(
        page = 1,
        movies = movieConverter.mapOutbound(list)
    )

}
