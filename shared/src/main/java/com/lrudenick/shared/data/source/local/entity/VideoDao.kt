package com.lrudenick.shared.data.source.local.entity

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * The Data Access Object for the [VideoEntity] class.
 */
@Dao
interface VideoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(videos: List<VideoEntity>)

    @Query("SELECT *, videoId FROM video")
    fun getAllVideos(): List<VideoEntity>
}
