package com.lrudenick.shared.data.source.local.entity

import androidx.room.*

/**
 * The Data Access Object for the [MovieDetailEntity] class.
 */
@Dao
interface MovieDetailDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movieDetail: MovieDetailEntity)

    @Query("SELECT *, movieId FROM movieDetail WHERE movieId=:movieId")
    fun getMovieDetail(movieId: Int): MovieDetailEntity

    @Transaction
    @Query("SELECT * FROM movieDetail WHERE movieId =:movieId")
    fun getMovieWithVideos(movieId: Int): MovieWithVideos
}
