package com.lrudenick.shared.data.source.remote.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * Interceptor used to add headers (like the Authorization Header) to HTTP requests.
 */
class ApiKeyInterceptor(
    environment: Environment
) : Interceptor {


    private val headers: MutableMap<String, String> =
        mutableMapOf(
            "Authorization" to environment.accessToken
        )

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder: Request.Builder = chain.request().newBuilder()
        for (entry in headers) {
            requestBuilder.addHeader(entry.key, entry.value)
        }
        return chain.proceed(requestBuilder.build())
    }
}
