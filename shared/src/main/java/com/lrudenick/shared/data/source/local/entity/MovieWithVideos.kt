package com.lrudenick.shared.data.source.local.entity

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class MovieWithVideos(
    @Embedded val movie: MovieDetailEntity,
    @Relation(
        parentColumn = "movieId",
        entityColumn = "videoId",
        associateBy = Junction(MovieVideoCrossRefEntity::class)
    )
    val videos: List<VideoEntity>
)
