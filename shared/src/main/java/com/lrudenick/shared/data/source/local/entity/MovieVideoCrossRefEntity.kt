package com.lrudenick.shared.data.source.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.lrudenick.shared.domain.model.MovieDetail
import com.lrudenick.shared.domain.model.Video

/**
 * This class represents [MovieDetail] and [Video] data.
 *
 * The [ColumnInfo] name is explicitly declared to allow flexibility for renaming the data class
 * properties without requiring changing the column name.
 */
@Entity(tableName = "movieVideoCrossRef", primaryKeys = ["movieId", "videoId"])
data class MovieVideoCrossRefEntity(
    @ColumnInfo(name = "movieId")
    val movieId: Int,

    @ColumnInfo(name = "videoId")
    val videoId: String
)
