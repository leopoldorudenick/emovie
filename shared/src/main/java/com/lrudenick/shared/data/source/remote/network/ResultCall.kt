package com.lrudenick.shared.data.source.remote.network

import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response
import java.lang.reflect.Type

/**
 * A [Call] for wrapping a network request into a [NetworkResult].
 */
class ResultCall<T>(
    private val backingCall: Call<T>,
    private val successType: Type,
) : Call<Result<T>> {
    override fun clone(): Call<Result<T>> = ResultCall(backingCall, successType)

    override fun execute(): Response<Result<T>> =
        throw UnsupportedOperationException("This call can't be executed synchronously")

    override fun enqueue(callback: Callback<Result<T>>) = backingCall.enqueue(object : Callback<T> {
        override fun onResponse(call: Call<T>, response: Response<T>) {
            callback.onResponse(
                this@ResultCall,
                if (response.isSuccessful) {
                    val body = response.body()
                    if (body == null) {
                        if (successType == Unit::class.java) {
                            @Suppress("UNCHECKED_CAST")
                            Response.success(Result.success(Unit as T), response.raw())
                        } else {
                            Response.success(Result.failure(IllegalStateException("Unexpected null body!")))
                        }
                    } else {
                        Response.success(Result.success(body), response.raw())
                    }
                } else {
                    Response.success(Result.failure(HttpException(response)))
                }
            )
        }

        override fun onFailure(call: Call<T>, t: Throwable) {
            callback.onResponse(this@ResultCall, Response.success(Result.failure(t)))
        }
    })

    override fun isExecuted(): Boolean = backingCall.isExecuted
    override fun cancel() = backingCall.cancel()
    override fun isCanceled(): Boolean = backingCall.isCanceled
    override fun request(): Request = backingCall.request()
    override fun timeout(): Timeout = backingCall.timeout()
}

