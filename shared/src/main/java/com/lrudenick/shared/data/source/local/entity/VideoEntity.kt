package com.lrudenick.shared.data.source.local.entity

import androidx.room.*

/**
 * This class represents [Video] data.
 *
 * The [ColumnInfo] name is explicitly declared to allow flexibility for renaming the data class
 * properties without requiring changing the column name.
 */
@Entity(tableName = "video")
data class VideoEntity(

    @PrimaryKey
    @ColumnInfo(name = "videoId")
    val videoId: String,

    @ColumnInfo(name = "key")
    val key: String,

    @ColumnInfo(name = "official")
    val official: Boolean,

    @ColumnInfo(name = "site")
    val site: String,

    @ColumnInfo(name = "type")
    val type: String,
)
