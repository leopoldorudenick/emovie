package com.lrudenick.shared.data.source.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CloudVideosResponse(
    @SerialName("id") val id: Int,
    @SerialName("results") val videos: List<CloudVideo>
)
