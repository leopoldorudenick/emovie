package com.lrudenick.shared.data.source.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CloudMovieResponse(
    @SerialName("page") val page: Int,
    @SerialName("results") val results: List<CloudMovie>
)
