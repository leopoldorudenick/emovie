package com.lrudenick.shared.data.source.remote.network

import com.lrudenick.shared.BuildConfig
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Models an environment for the app's services to interact with.
 */
data class Environment(
    val apiUrl: HttpUrl,
    val accessToken: String,
    val imgUrlPath: String
)

/**
 * Interface for providing an Environment.
 */
interface EnvironmentProvider {

    /**
     * Returns the current [Environment].
     */
    operator fun invoke(): Environment
}

private val ENV_PROD = Environment(
    apiUrl = "https://api.themoviedb.org".toHttpUrl(),
    accessToken = BuildConfig.TMDB_READ_ACCESS_TOKEN,
    imgUrlPath = "https://image.tmdb.org/t/p/w600_and_h900_bestv2"
)

/**
 * Injectable class for providing production environment.
 */
@Singleton
open class ProdEnvironmentProvider @Inject constructor() : EnvironmentProvider {
    override fun invoke(): Environment = ENV_PROD
}
