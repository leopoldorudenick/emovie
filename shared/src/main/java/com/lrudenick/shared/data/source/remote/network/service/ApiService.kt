package com.lrudenick.shared.data.source.remote.network.service

import com.lrudenick.shared.data.source.remote.model.CloudMovieDetail
import com.lrudenick.shared.data.source.remote.model.CloudMovieResponse
import com.lrudenick.shared.data.source.remote.model.CloudVideosResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("/3/movie/upcoming")
    suspend fun getUpcomingMovies(): CloudMovieResponse

    @GET("/3/movie/top_rated")
    suspend fun getTopRatedMovies(): CloudMovieResponse

    @GET("/3/movie/{movieId}/recommendations")
    suspend fun getRecommendations(@Path("movieId") movieId: Int): CloudMovieResponse

    @GET("/3/movie/{movieId}")
    suspend fun getMovieDetail(@Path("movieId") movieId: Int): CloudMovieDetail

    @GET("/3/movie/{movieId}/videos")
    suspend fun getMovieVideos(@Path("movieId") movieId: Int): CloudVideosResponse
}
