package com.lrudenick.shared.data.source.remote.network

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.util.concurrent.TimeUnit

/**
 * Interceptor used to dynamically setup the request timeout.
 */
class TimeoutInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()
        var connectTimeout: Int = chain.connectTimeoutMillis() / 1000
        var readTimeout: Int = chain.readTimeoutMillis() / 1000
        var writeTimeout: Int = chain.writeTimeoutMillis() / 1000

        val newConnectTimeout: String? = request.header(HEADER_CONNECT_TIMEOUT_SECONDS)
        val newReadTimeout: String? = request.header(HEADER_READ_TIMEOUT_SECONDS)
        val newWriteTimeout: String? = request.header(HEADER_WRITE_TIMEOUT_SECONDS)

        if (!newConnectTimeout.isNullOrBlank()) {
            connectTimeout = newConnectTimeout.toInt()
        }
        if (!newReadTimeout.isNullOrBlank()) {
            readTimeout = newReadTimeout.toInt()
        }
        if (!newWriteTimeout.isNullOrBlank()) {
            writeTimeout = newWriteTimeout.toInt()
        }

        val requestBuilder: Request.Builder = request.newBuilder()
        requestBuilder.removeHeader(HEADER_CONNECT_TIMEOUT_SECONDS)
        requestBuilder.removeHeader(HEADER_READ_TIMEOUT_SECONDS)
        requestBuilder.removeHeader(HEADER_WRITE_TIMEOUT_SECONDS)

        return chain
            .withConnectTimeout(connectTimeout, TimeUnit.SECONDS)
            .withReadTimeout(readTimeout, TimeUnit.SECONDS)
            .withWriteTimeout(writeTimeout, TimeUnit.SECONDS)
            .proceed(requestBuilder.build())
    }

    companion object {
        const val HEADER_CONNECT_TIMEOUT_SECONDS = "Connect-Timeout"
        const val HEADER_READ_TIMEOUT_SECONDS = "Read-Timeout"
        const val HEADER_WRITE_TIMEOUT_SECONDS = "Write-Timeout"
    }
}
