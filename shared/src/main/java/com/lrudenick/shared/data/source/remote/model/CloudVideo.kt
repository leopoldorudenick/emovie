package com.lrudenick.shared.data.source.remote.model

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CloudVideo(
    @SerialName("id") val id: String,
    @SerialName("key") val key: String,
    @SerialName("official") val official: Boolean,
    @SerialName("site") val site: String,
    @SerialName("type") val type: String
)
