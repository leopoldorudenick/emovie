package com.lrudenick.shared.data.source.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lrudenick.shared.data.source.local.entity.*

/**
 * The [Room] database for this app.
 */
@Database(
    entities = [
        MovieDetailEntity::class,
        MovieEntity::class,
        VideoEntity::class,
        MovieVideoCrossRefEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDetailDao(): MovieDetailDao
    abstract fun movieDao(): MovieDao
    abstract fun videoDao(): VideoDao
    abstract fun movieVideoCrossRefDao(): MovieVideoCrossRefDao

    companion object {
        private const val databaseName = "emovie-db"

        fun buildDatabase(context: Context): AppDatabase {
            // https://medium.com/androiddevelopers/understanding-migrations-with-room-f01e04b07929
            return Room.databaseBuilder(context, AppDatabase::class.java, databaseName)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}
