package com.lrudenick.shared.util

/**
 * Wrapper around [kotlinx.coroutines.flow.SharedFlow] that clears itself when collecting values.
 * This ensures they are only delivered once.
 */
interface EventFlow<T : Any> {
    /**
     * Gets the last emitted event. Useful for testing.
     */
    val value: T?

    /**
     * @see [kotlinx.coroutines.flow.collect]
     */
    suspend fun collect(action: suspend (T) -> Unit)
}
