package com.lrudenick.shared.util

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

/**
 * Observe flow changes in a fragment/activity lifecycle.
 */
fun <T> Flow<T>.observe(owner: LifecycleOwner, action: suspend (T) -> Unit) =
    onEach(action).launchIn(owner.lifecycleScope)

/**
 * Observe event flow changes in a fragment/activity lifecycle.
 */
fun <T : Any> EventFlow<T>.observe(owner: LifecycleOwner, action: suspend (T) -> Unit) =
    owner.lifecycleScope.launchWhenStarted {
        collect(action)
    }
