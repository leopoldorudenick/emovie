package com.lrudenick.shared.domain.model

/**
 * Model class for Movie data
 */
data class Movie(
    /**
     * Unique identifier
     */
    val id: Int,
    /**
     * Artist name
     */
    val title: String,
    /**
     * For adult
     */
    val adult: Boolean,
    /**
     * Plot text
     */
    val plot: String,
    /**
     * Poster image url
     */
    val posterImg: String? = null,
    /**
     * Original language
     */
    val language: String,
    /**
     * Average rate
     */
    val rate: Double,
    /**
     * Release date
     */
    val releaseDate: String
)

enum class MovieFilter {

    ALL,

    IN_ENGLISH,

    IN_HINDI,

    RELEASED_IN_2003
}
