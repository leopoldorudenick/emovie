package com.lrudenick.shared.domain.repository

import com.lrudenick.shared.domain.model.MovieDetail
import com.lrudenick.shared.domain.model.MoviesResponse
import com.lrudenick.shared.domain.model.VideosResponse

interface MovieRepository {

    suspend fun getUpcomingMovies(): Result<MoviesResponse>

    suspend fun getTopRatedMovies(): Result<MoviesResponse>

    suspend fun getRecommendations(movieId: Int): Result<MoviesResponse>

    suspend fun getMovieDetail(movieId: Int): Result<MovieDetail>

    suspend fun getVideos(movieId: Int): Result<VideosResponse>

}
