package com.lrudenick.shared.domain.use.movieList

import com.lrudenick.shared.domain.model.MoviesResponse
import kotlinx.coroutines.CoroutineScope

interface MovieList {
    interface In {
        fun getTopRatedMovies()
        fun getUpcomingMovies()
        fun getRecommendedMovies(movieId: Int)
    }

    interface Out : CoroutineScope {

        fun setProcessState(state: MovieListState)

        fun setTopRatedMovies(movies: MoviesResponse)

        fun setUpcomingMovies(movies: MoviesResponse)

        fun setRecommendedMovies(movies: MoviesResponse)

    }

    sealed class MovieListState {
        object Idle : MovieListState()
        object Loading : MovieListState()
        object Loaded : MovieListState()
        class Failed(val throwable: Throwable) : MovieListState()
    }
}
