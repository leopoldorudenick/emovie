package com.lrudenick.shared.domain.use.movieDetail

import com.lrudenick.shared.di.IoDispatcher
import com.lrudenick.shared.domain.ScopedUseCase
import com.lrudenick.shared.domain.repository.MovieRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import timber.log.Timber

class MovieDetailUseCase @AssistedInject constructor(
    @Assisted override val out: MovieDetailUse.Out,
    private val movieRepository: MovieRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : MovieDetailUse.In, ScopedUseCase() {

    override fun getMovieDetail(movieId: Int) {
        Timber.d("Fetching movie detail")
        out.setProcessState(MovieDetailUse.MovieDetailState.Loading)

        launch(ioDispatcher) {
            try {
                val response = movieRepository.getMovieDetail(movieId).getOrThrow()
                out.setMovieDetail(response)
                out.setProcessState(MovieDetailUse.MovieDetailState.Loaded)
                Timber.d("Movies detail fetched successfully")
            } catch (e: Exception) {
                Timber.e("Error fetching movie detail with id: $movieId", e)
                out.setProcessState(MovieDetailUse.MovieDetailState.Failed(e))
            }
        }
    }

    override fun getVideos(movieId: Int) {
        Timber.d("Fetching movie videos")
        out.setProcessState(MovieDetailUse.MovieDetailState.Loading)

        launch(ioDispatcher) {
            try {
                val response = movieRepository.getVideos(movieId).getOrThrow()
                out.setVideoId(response.videos[0].key)
                out.setProcessState(MovieDetailUse.MovieDetailState.Loaded)
                Timber.d("Movies videos fetched successfully")
            } catch (e: Exception) {
                Timber.e("Error fetching movie videos with id: $movieId", e)
                out.setProcessState(MovieDetailUse.MovieDetailState.Failed(e))
            }
        }
    }

}
