package com.lrudenick.shared.domain.model

data class VideosResponse(
    val id: Int,
    val videos: List<Video>
)
