package com.lrudenick.shared.domain.use.movieList

import dagger.assisted.AssistedFactory

@AssistedFactory
interface MovieListFactory {
    fun create(out: MovieList.Out): MovieListUseCase
}
