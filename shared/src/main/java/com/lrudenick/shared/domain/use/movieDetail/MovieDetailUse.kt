package com.lrudenick.shared.domain.use.movieDetail

import com.lrudenick.shared.domain.model.MovieDetail
import kotlinx.coroutines.CoroutineScope

interface MovieDetailUse {
    interface In {
        fun getMovieDetail(movieId: Int)

        fun getVideos(movieId: Int)
    }

    interface Out : CoroutineScope {

        fun setProcessState(state: MovieDetailState)

        fun setMovieDetail(movieDetail: MovieDetail)

        fun setVideoId(videoId: String)

    }

    sealed class MovieDetailState {
        object Idle : MovieDetailState()
        object Loading : MovieDetailState()
        object Loaded : MovieDetailState()
        class Failed(val throwable: Throwable) : MovieDetailState()
    }
}
