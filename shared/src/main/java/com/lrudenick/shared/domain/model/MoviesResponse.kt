package com.lrudenick.shared.domain.model

/**
 * Model class for Artist response data
 */
data class MoviesResponse(
    /**
     * Page
     */
    val page: Int,
    /**
     * Movies list
     */
    val movies: List<Movie>
)
