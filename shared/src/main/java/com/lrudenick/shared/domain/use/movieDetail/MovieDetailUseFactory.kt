package com.lrudenick.shared.domain.use.movieDetail

import dagger.assisted.AssistedFactory

@AssistedFactory
interface MovieDetailUseFactory {
    fun create(out: MovieDetailUse.Out): MovieDetailUseCase
}
