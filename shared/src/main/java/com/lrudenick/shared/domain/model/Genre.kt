package com.lrudenick.shared.domain.model

data class Genre(
    /**
     * Unique identifier
     */
    val id: Int,
    /**
     * Name
     */
    val name: String
)
