package com.lrudenick.shared.domain

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job

abstract class ScopedUseCase(
): CoroutineScope {
    protected abstract val out: CoroutineScope
    override val coroutineContext by lazy { Job() + out.coroutineContext }
}
