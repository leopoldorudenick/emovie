package com.lrudenick.shared.domain.model

data class Video(
    val id: String,
    val key: String,
    val official: Boolean,
    val site: String,
    val type: String
)
