package com.lrudenick.shared.domain.use.movieList

import com.lrudenick.shared.di.IoDispatcher
import com.lrudenick.shared.domain.ScopedUseCase
import com.lrudenick.shared.domain.repository.MovieRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import timber.log.Timber

class MovieListUseCase @AssistedInject constructor(
    @Assisted override val out: MovieList.Out,
    private val movieRepository: MovieRepository,
    @IoDispatcher private val ioDispatcher: CoroutineDispatcher
) : MovieList.In, ScopedUseCase() {

    override fun getTopRatedMovies() {
        Timber.d("Fetching movies")
        out.setProcessState(MovieList.MovieListState.Loading)

        launch(ioDispatcher) {
            try {
                val response = movieRepository.getTopRatedMovies().getOrThrow()
                out.setTopRatedMovies(response)
                out.setProcessState(MovieList.MovieListState.Loaded)
                Timber.d("Top rated movies fetched successfully")
            } catch (e: Exception) {
                Timber.e("Error fetching top rated movies", e)
                out.setProcessState(MovieList.MovieListState.Failed(e))
            }
        }
    }

    override fun getUpcomingMovies() {
        Timber.d("Fetching movies")
        out.setProcessState(MovieList.MovieListState.Loading)

        launch(ioDispatcher) {
            try {
                val response = movieRepository.getUpcomingMovies().getOrThrow()
                out.setUpcomingMovies(response)
                out.setProcessState(MovieList.MovieListState.Loaded)
                Timber.d("Upcoming movies fetched successfully")
            } catch (e: Exception) {
                Timber.e("Error fetching upcoming movies", e)
                out.setProcessState(MovieList.MovieListState.Failed(e))
            }
        }
    }

    override fun getRecommendedMovies(movieId: Int) {
        Timber.d("Fetching movies")
        out.setProcessState(MovieList.MovieListState.Loading)

        launch(ioDispatcher) {
            try {
                val response = movieRepository.getRecommendations(movieId).getOrThrow()
                out.setRecommendedMovies(response)
                out.setProcessState(MovieList.MovieListState.Loaded)
                Timber.d("Recommended movies fetched successfully")
            } catch (e: Exception) {
                Timber.e("Error fetching recommended movies", e)
                out.setProcessState(MovieList.MovieListState.Failed(e))
            }
        }
    }

}
