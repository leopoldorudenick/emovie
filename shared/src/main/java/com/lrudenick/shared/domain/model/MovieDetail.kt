package com.lrudenick.shared.domain.model

/**
 * Model class for MovieDetail data
 */
data class MovieDetail(
    /**
     * Unique identifier
     */
    val id: Int,
    /**
     * Artist name
     */
    val title: String,
    /**
     * For adult
     */
    val adult: Boolean,
    /**
     * Plot text
     */
    val plot: String,
    /**
     * Poster image url
     */
    val posterImg: String? = null,
    /**
     * Original language
     */
    val language: String,
    /**
     * Average rate
     */
    val rate: Double,
    /**
     * Release date
     */
    val releaseDate: String,
    /**
     * Genres separated by '-'
     */
    val genres: String
)

fun MovieDetail.releaseYear() = try {
    releaseDate.split("-")[0]
} catch (e: Exception) {
    ""
}

fun MovieDetail.genresList() = genres.split("-").joinToString(separator = " \u2022 ")
