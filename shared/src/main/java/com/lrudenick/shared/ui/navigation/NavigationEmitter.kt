package com.lrudenick.shared.ui.navigation

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData

/**
 * An interface for an object that owns a set of events that can be
 * observed similar to a standard [LiveData]. See [NavigationViewModel]
 * for more usage details.
 */
interface NavigationEmitter<T> {
    val events: EventBus<T>

    fun observeEvents(lifecycleOwner: LifecycleOwner, observer: (T)->Unit) {
        events.observe(lifecycleOwner, { it.get()?.also(observer) })
    }
}
