package com.lrudenick.shared.ui.navigation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

typealias EventBus<T> = LiveData<Event<T>>
typealias MutableEventBus<T> = MutableLiveData<Event<T>>

/**
 * Used as a wrapper for data that is exposed via a LiveData that represents an event.
 * Events can only have [get] run once. Subsequent calls return null.
 * This is an easy way to ensure events are consumed once.
 */
open class Event<out T>(private val content: T) {

    private var hasBeenHandled = false

    /**
     * Returns the content and prevents its use again.
     */
    fun get(): T? {
        return if (hasBeenHandled) {
            null
        } else {
            hasBeenHandled = true
            content
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    fun peekContent(): T = content
}
