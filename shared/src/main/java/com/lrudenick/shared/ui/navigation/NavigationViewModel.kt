package com.lrudenick.shared.ui.navigation

import com.lrudenick.shared.binding.ObservableViewModel


/**
 * [NavigationViewModel] extends the existing ObservableViewModel for ViewModels that require
 * some type of interaction with Fragment actions. Usually, these are navigation events
 * managed by the Fragment. In this case, a [NavigationViewModel] can be used with
 * an event type [T]. This type can be as simple as a String or Int, or a custom enum
 * class or sealed class. In the Fragment setup, the events can be observed
 * using [NavigationEmitter.observeEvents].
 *
 * These events rely on [Event] to ensure one-time delivery of each emitted event, making
 * it safe for navigation transitions or intent launches.
 */
open class NavigationViewModel<T> :
    NavigationEmitter<T>,
    ObservableViewModel() {
    override var events = MutableEventBus<T>()

    fun emit(value: T) {
        events.value = Event(value)
    }
}
