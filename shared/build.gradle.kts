import java.io.FileInputStream
import java.io.InputStreamReader
import java.util.*

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize")
    id("kotlinx-serialization")
    id("dagger.hilt.android.plugin")
    id("com.vanniktech.android.junit.jacoco")
}

class PropertiesLoader {

    fun loadProperties(propertiesPath: String): Properties {
        val properties = Properties()
        val localProperties = File(propertiesPath)

        if (localProperties.isFile) {
            InputStreamReader(FileInputStream(localProperties), Charsets.UTF_8)
                .use { reader ->
                    properties.load(reader)
                }
        }
        return properties
    }
}

val buildProperties = PropertiesLoader().loadProperties("$rootDir/secrets.properties")

android {
    compileSdk = AndroidSdk.compile
    defaultConfig {
        minSdk = AndroidSdk.min
        targetSdk = AndroidSdk.target

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        buildConfigField(
            "String", "TMDB_READ_ACCESS_TOKEN", buildProperties.getProperty("tmdb_read_access_token")
        )
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
        dataBinding = true
    }
}

dependencies {

    with(Deps.Material) {
        implementation(material)
    }
    with(Deps.Androidx) {
        implementation(appcompat)
        implementation(constraintLayout)
        implementation(lifecycleLivedata)
        implementation(lifecycleViewmodel)
        implementation(navigationFragment)
        implementation(navigationUi)
    }
    with(Deps.Dagger) {
        implementation(hiltAndroid)
        kapt(hiltAndroidCompiler)
    }
    with(Deps.Glide) {
        implementation(glide)
        annotationProcessor(glideCompiler)
    }
    with(Deps.Kotlinx) {
        implementation(coroutinesCore)
        implementation(serializationCore)
        implementation(serializationRuntime)
        testImplementation(serializationRuntime)
        testImplementation(coroutinesTest)
    }
    with(Deps.Retrofit) {
        implementation(retrofitRuntime)
        implementation(retrofit2Converter)
    }
    with(Deps.OkHttp) {
        implementation(okHttpRuntime)
        implementation(okHttpLoggingInterceptor)
    }
    with(Deps.Timber) {
        implementation(timberLogging)
    }
    with(Deps.Room) {
        implementation(roomKtx)
        implementation(roomRuntime)
        kapt(roomCompiler)
        testImplementation(roomKtx)
        testImplementation(roomRuntime)
    }
    with(Deps.Junit) {
        testImplementation(junit)
    }
}

// Jacoco reporting:
junitJacoco {
    jacocoVersion = "0.8.7"
    includeNoLocationClasses = false
    excludes = listOf(
        // Databinding
        "**/DataBinderMapperImpl*.*",
        "**/DataBindingInfo*.*",
        "**/MobileNavigationDirections*.*",
        "android/databinding/**/*.class",
        "**/android/databinding/*Binding.class",
        "**/BR.*",
        "**/databinding/**",

        // Android
        "**/R.class",
        "**/R\$*.class",
        "**/BuildConfig.*",
        "**/Manifest*.*",
        "**/android/**",

        // Hilt
        "**/*_MembersInjector.class",
        "**/*_Factory.*",
        "**/*InstanceHolder*",
        "**/*ModuleDeps*",
        "**/*_AssistedFactory.*",
        "**/*Hilt*",
        "**/**ModuleDeps",
        "**/di/**",

        // JNI:
        "**/jni/**",

        // extensions
        "**/extensions/**",
    )
}
