object Versions {

    const val kotlin = "1.6.10"

    const val kotlinCoroutines = "1.6.0"

    const val kotlinSerialization = "1.3.2"

    const val appcompat = "1.4.1"

    const val lifecycle = "2.4.0"

    const val constraintlayout = "2.1.3"

    const val navigation = "2.4.1"

    const val material = "1.5.0"

    const val hiltAndroid = "2.41"

    const val coroutinesTest = "1.6.0"

    const val coreTest = "2.1.0"

    const val junit = "4.13.2"

    const val glide = "4.13.0"

    const val legacySupport = "1.0.0"

    const val androidGradleTools = "7.1.0"

    const val jacocoPlugin = "0.16.0"

    const val retrofit = "2.9.0"

    const val retrofit2Converter = "0.8.0"

    const val okHttp = "4.9.1"

    const val timber = "5.0.1"

    const val room = "2.4.2"
}

object AndroidSdk {
    const val min = 24
    const val compile = 31
    const val target = compile
}

object Deps {

    object RootBuild {
        const val kotlinGradle =
            "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
        const val androidGradle =
            "com.android.tools.build:gradle:${Versions.androidGradleTools}"
        const val hiltGradle =
            "com.google.dagger:hilt-android-gradle-plugin:${Versions.hiltAndroid}"
        const val safeArgsPlugin =
            "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.navigation}"
        const val jacocoPlugin =
            "com.vanniktech:gradle-android-junit-jacoco-plugin:${Versions.jacocoPlugin}"
        const val kotlinxSerialization =
            "org.jetbrains.kotlin:kotlin-serialization:${Versions.kotlin}"
    }

    object Androidx {
        const val appcompat = "androidx.appcompat:appcompat:${Versions.appcompat}"
        const val constraintLayout =
            "androidx.constraintlayout:constraintlayout:${Versions.constraintlayout}"
        const val lifecycleLivedata =
            "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
        const val lifecycleViewmodel =
            "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"
        const val navigationFragment =
            "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
        const val navigationUi =
            "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
        const val coreTesting = "androidx.arch.core:core-testing:${Versions.coreTest}"
    }

    object Material {
        const val material = "com.google.android.material:material:${Versions.material}"
    }

    object Dagger {
        const val hiltAndroid = "com.google.dagger:hilt-android:${Versions.hiltAndroid}"
        const val hiltAndroidCompiler =
            "com.google.dagger:hilt-android-compiler:${Versions.hiltAndroid}"
    }

    object Kotlinx {
        const val coroutinesCore =
            "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.kotlinCoroutines}"
        const val serializationCore =
            "org.jetbrains.kotlinx:kotlinx-serialization-core:${Versions.kotlinSerialization}"
        const val serializationRuntime =
            "org.jetbrains.kotlinx:kotlinx-serialization-json:${Versions.kotlinSerialization}"
        const val coroutinesTest =
            "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.coroutinesTest}"
    }

    object Junit {
        const val junit = "junit:junit:${Versions.junit}"
    }

    object Glide {
        const val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
        const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glide}"
    }

    object Legacy {
        const val legacySupport = "androidx.legacy:legacy-support-v4:${Versions.legacySupport}"
    }

    object Retrofit {
        const val retrofitRuntime = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"

        // Kotlinx-serialization converter
        const val retrofit2Converter =
            "com.jakewharton.retrofit:retrofit2-kotlinx-serialization-converter:${Versions.retrofit2Converter}"
    }

    object OkHttp {
        const val okHttpRuntime = "com.squareup.okhttp3:okhttp:${Versions.okHttp}"
        const val okHttpLoggingInterceptor =
            "com.squareup.okhttp3:logging-interceptor:${Versions.okHttp}"
    }

    object Timber {
        const val timberLogging = "com.jakewharton.timber:timber:${Versions.timber}"
    }

    object Room {
        const val roomCompiler = "androidx.room:room-compiler:${Versions.room}"
        const val roomKtx = "androidx.room:room-ktx:${Versions.room}"
        const val roomRuntime = "androidx.room:room-runtime:${Versions.room}"
    }

}
